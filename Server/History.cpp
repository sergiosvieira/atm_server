#include "History.h"

CHistory::CHistory()
{
}

CHistory::CHistory(unsigned int id, HistoryOperation operation, float value):
	m_id(id), m_operation(operation), m_value(value)
{
}

unsigned int CHistory::id()
{
	return m_id;
}

CHistory::HistoryOperation CHistory::operation()
{
	return m_operation;
}

float CHistory::value()
{
	return m_value;
}
