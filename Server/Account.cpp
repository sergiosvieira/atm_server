#include "Account.h"

CAccount::CAccount(int id, float balance)
{
	m_id = id;
	m_balance = balance;
}

int CAccount::id()
{
	return m_id;
}

float CAccount::balance()
{
	return m_balance;
}

void CAccount::balance(float balance)
{
	m_balance = balance;
}
CAccount::VHistory & CAccount::histories()
{
	return m_histories;
}
