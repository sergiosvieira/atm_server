#include "AccountManager.h"

#include <memory>
#include "Client.h"
#include "Account.h"
#include "History.h"

CAccountManager::AccountError CAccountManager::deposit(CClient * client, float value)
{
	AccountError result = AccountError::NONE;
	CAccount* acc = client->account();
	CAccount::VHistory& entries = acc->histories();
	unsigned int id = entries.size() + 1;
	acc->balance(acc->balance() + value);
	CHistory* entry = new CHistory(id, CHistory::HistoryOperation::CREDIT, value);
	entries.push_back(std::unique_ptr<CHistory>(entry));	
	return result;
}

CAccountManager::AccountError CAccountManager::debit(CClient * client, float value)
{
	AccountError result = AccountError::NONE;
	CAccount* acc = client->account();
	CAccount::VHistory& entries = acc->histories();
	unsigned int id = entries.size() + 1;
	if (acc->balance() - value < 0)
	{
		return AccountError::WITHOUT_BALANCE;
	}
	CHistory* entry = new CHistory(id, CHistory::HistoryOperation::DEBIT, value);
	entries.push_back(std::unique_ptr<CHistory>(entry));
	return result;
}
