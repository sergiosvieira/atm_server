#pragma once

#include <string>

class CHistory
{
public:
	typedef enum
	{
		DEBIT,
		CREDIT
	} HistoryOperation;
	CHistory();
	CHistory(unsigned int id, HistoryOperation operation, float value);
	unsigned int id();
	HistoryOperation operation();
	float value();
protected:
	unsigned int m_id = 0;
	HistoryOperation m_operation;
	float m_value = 0.f;
};

