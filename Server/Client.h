#pragma once

#include <string>

class CAccount;

class CClient
{
public:
	CClient(std::string name, 
			std::string pin, 
			std::string password);
	~CClient();
	std::string name();
	std::string pin();
	std::string password();
	void password(std::string password);
	CAccount* account();
protected:
	std::string m_name;
	std::string m_pin;
	std::string m_password;
	CAccount* m_account;
};

