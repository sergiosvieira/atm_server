#pragma once

class CClient;

class CAccountManager
{
public:
	typedef enum
	{
		NONE,
		WITHOUT_BALANCE
	} AccountError;
	static AccountError deposit(CClient * client, float value);
	static AccountError debit(CClient * client, float value);
};

