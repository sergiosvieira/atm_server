#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <iostream>
#include <fstream>
#include <random>
#include <memory>
#include <codecvt>

#ifdef _WIN32
#define NOMINMAX
#include <Windows.h>
#else
# include <sys/time.h>
#endif

#include "cpprest/json.h"
#include "cpprest/http_listener.h"
#include "cpprest/uri.h"
#include "cpprest/asyncrt_utils.h"

using namespace web;
using namespace http;
using namespace utility;
using namespace http::experimental::listener;
using namespace std;

#include "Client.h"

std::vector<std::unique_ptr<CClient>> database;

void initialize()
{
	CClient* client = new CClient("Sergio Vieira", "1234", "4321");
	database.push_back(std::unique_ptr<CClient>(client));
}

CClient* searchClient(std::string pin)
{
	struct find_pin : std::unary_function<std::unique_ptr<CClient>, bool> {
		std::string m_pin;
		find_pin(std::string pin) :m_pin(pin) { }
		bool operator()(std::unique_ptr<CClient>& m) const {			
			std::string str = m.get()->pin();
			 return str == m_pin;
		}
	};
	std::vector<std::unique_ptr<CClient>>::iterator it = std::find_if(database.begin(), database.end(), find_pin(pin));
	if (it != database.end())
	{
		return it->get();
	}
	return nullptr;
}

string ws2s(const std::wstring& wstr)
{
	typedef std::codecvt_utf8<wchar_t> convert_typeX;
	std::wstring_convert<convert_typeX, wchar_t> converterX;
	return converterX.to_bytes(wstr);
}
class ATMServer
{
public:
	ATMServer() 
	{		
	}
	ATMServer(utility::string_t url): m_listener(url)
	{
		m_listener.support(methods::GET, std::bind(&ATMServer::handle_get, this, std::placeholders::_1));
		m_listener.support(methods::PUT, std::bind(&ATMServer::handle_put, this, std::placeholders::_1));
		m_listener.support(methods::POST, std::bind(&ATMServer::handle_post, this, std::placeholders::_1));
		m_listener.support(methods::DEL, std::bind(&ATMServer::handle_delete, this, std::placeholders::_1));
	}
	pplx::task<void> open() { return m_listener.open(); }
	pplx::task<void> close() { return m_listener.close(); }
private:
	void handle_get(http_request message)
	{
	}
	void handle_put(http_request message)
	{
	}
	void handle_post(http_request message)
	{
		//ucout << message.to_string() << std::endl;
		message.extract_json().then([=](json::value obj){
			std::wstring pin = obj[L"user"].as_string();
			std::string str = ws2s(pin);
			CClient* client = searchClient(str);
			if (client != nullptr)
			{
				json::value answer;
				std::string name = client->name();
				answer[L"user_name"] = json::value::string(utility::conversions::to_string_t(name));
				answer[L"pin"] = json::value::string(utility::conversions::to_string_t(pin));
				ucout << answer.serialize() << std::endl;
				message.reply(status_codes::OK, answer);
			}
			else
			{
				message.reply(status_codes::NotFound, json::value::string(L"Pin not registered!"));
			}
		});
	}
	void handle_delete(http_request message)
	{
	}
	http_listener m_listener;
};

std::unique_ptr<ATMServer> g_httpDealer;


void on_initialize(const string_t& address)
{
	uri_builder uri(address);
	uri.append_path(U("atm/service"));
	auto addr = uri.to_uri().to_string();
	g_httpDealer = std::unique_ptr<ATMServer>(new ATMServer(addr));
	try
	{
		g_httpDealer->open().then([=](){wcout << "listening at " << addr << "\n";}).wait();
		while (true);
	}
	catch (exception const & e)
	{
		wcout << e.what() << endl;
	}
	return;
}

void on_shutdown()
{
	g_httpDealer->close().wait();
	return;
}

#ifdef _WIN32
int wmain(int argc, wchar_t *argv[])
#else
int main(int argc, char *argv[])
#endif
{
	initialize();
	utility::string_t port = U("34568");
	if (argc == 2)
	{
		port = argv[1];
	}
	utility::string_t address = U("http://localhost:");
	address.append(port);
	on_initialize(address);
	std::string line;
	std::getline(std::cin, line);
	on_shutdown();
	return 0;
}