#pragma once

#include <memory>
#include <vector>
#include "History.h"

class CAccount
{
public:
	typedef std::vector<std::unique_ptr<CHistory>> VHistory;
	CAccount(int id, float balance);
	int id();
	float balance();
	void balance(float balance);
	VHistory& histories();
private:
	int m_id = 0;
	float m_balance = 0.0f;
	VHistory m_histories;
};