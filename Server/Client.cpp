#include "Client.h"

#include "Account.h"


CClient::CClient(std::string name, std::string pin, std::string password):
	m_name(name), m_pin(pin), m_password(password)
{
	m_account = new CAccount(1, 0.0);
}

CClient::~CClient()
{
	delete m_account;
}

std::string CClient::name()
{
	return m_name;
}

std::string CClient::pin()
{
	return m_pin;
}

std::string CClient::password()
{
	return m_password;
}

void CClient::password(std::string password)
{
	m_password = password;
}

CAccount * CClient::account()
{
	return m_account;
}
